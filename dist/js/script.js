/* global SimpleBar */

'use strict';

$(document).ready(function () {
  // Cookies
  (function () {
    var cookies = $('.cookies');

    if (getCookie('FUZE-diksi') === undefined) cookies.addClass('js-show');

    cookies.find('.btn--cookies').on('click', function (e) {
      e.preventDefault();
      cookies.removeClass('js-show');
      document.cookie = 'FUZE-diksi=value; path=/;';
    });

    function getCookie(name) {
      var matches = document.cookie.match(new RegExp("(?:^|; )".concat(
      name.replace(/([.$?*|{}()[]\\\/\+^])/g, '\\$1'), "=([^;]*)")));

      return matches ? decodeURIComponent(matches[1]) : undefined;
    }
  })();


  // Burger menu
  (function () {
    var header = $('#header');
    var burgerMenu = header.find('.burger-menu');

    burgerMenu.on('click', function (e) {
      e.preventDefault();
      var parent = $(this).parent();
      var menu = parent.find('.header__menu');
      menu.toggleClass('active');
      $('html').toggleClass('no-scroll');

      if (menu.hasClass('active') && $('html').hasClass('no-scroll')) {
        var width = $(window).width();

        $(window).on('resize.menu', function () {
          var widthResize = $(window).width();

          if (width !== widthResize) {
            menu.removeClass('active');
            $('html').removeClass('no-scroll');
            return $(window).off('resize.menu');
          }
        });
      } else {
        return $(window).off('resize.menu');
      }
    });

  })();


  // Select2
  (function () {
    var selects = $('select');

    selects.each(function () {
      var $this = $(this);
      var parent = $this.parent();
      var selectThems = parent.find('select[name="thems"]');
      var selectCitiy = parent.find('select[name="citiy"]');
      var selectRegion = parent.find('select[name="region"]');
      var selectStreet = parent.find('select[name="street"]');
      var selectHouse = parent.find('select[name="house"]');

      selectThems.select2({
        placeholder: 'Выберете тему',
        minimumResultsForSearch: -1,
        dropdownParent: parent });


      selectCitiy.select2({
        placeholder: 'Город',
        dropdownParent: parent,
        language: {
          noResults: function noResults() {
            return 'Город не найден';
          } } });



      selectRegion.select2({
        placeholder: 'Регион',
        dropdownParent: parent,
        language: {
          noResults: function noResults() {
            return 'Регион не найден';
          } } });



      selectStreet.select2({
        placeholder: 'Улица',
        dropdownParent: parent,
        language: {
          noResults: function noResults() {
            return 'Улица не найдена';
          } } });



      selectHouse.select2({
        placeholder: '№ дома',
        dropdownParent: parent,
        language: {
          noResults: function noResults() {
            return 'Дом не найден';
          } } });


    });
  })();


  // Init custom scroll
  (function () {
    var table = document.getElementById('tables');
    var tableWinners = document.getElementById('table-winners');

    if (table !== null) {
      var tablesProfile = table.getElementsByClassName('profile-table');

      for (var i = 0; i < tablesProfile.length; i++) {initCustomScroll(tablesProfile[i]);}
    }

    if (tableWinners !== null) initCustomScroll(tableWinners);

    function initCustomScroll(el) {
      return new SimpleBar(el, {
        autoHide: false });

    }
  })();


  // Set prizes
  (function () {
    var tableChecks = document.getElementById('tables');

    function setPrizes() {
      var prizes = tableChecks.getElementsByClassName('profile-page__prize');
      var status = tableChecks.getElementsByClassName('profile-page__status-prize-inner');

      for (var i = 0; i < status.length; i++) {status[i].setAttribute('data-prize', prizes[i].textContent);}
    }

    if (tableChecks !== null) setPrizes();
  })();


  // Tabs
  (function () {
    var tabs = $('#tabs');
    var wrap = $('#tables');
    var tables = wrap.find('.profile-table');
    var tabsItem = tabs.find('li');

    tabs.on('click', 'li', function () {
      var $this = $(this);
      var i = $this.index();

      tabsItem.removeClass('active');
      $this.addClass('active');
      tables.removeClass('active').eq(i).addClass('active');
    });
  })();


  // Popups
  (function () {
    var menu = $('#header').find('.header__menu');
    var btnOpenPopups = $('.js-popup-button');
    var btnClosePopups = $('.js-close-popup');

    function intPopup() {
      btnOpenPopups.on('click', function (e) {
        e.preventDefault();
        $('.popup').removeClass('js-popup-show');
        var popupClass = ".".concat($(this).attr('data-popupshow'));
        $(popupClass).addClass('js-popup-show');

        if (!menu.hasClass('active')) hiddenScroll();
      });

      closePopup();
    }

    function closePopup() {
      btnClosePopups.on('click', function (e) {
        e.preventDefault();
        $('.popup').removeClass('js-popup-show');

        if (!menu.hasClass('active')) visibleScroll();
      });
    }
    intPopup();
  })();

  function hiddenScroll() {
    if ($(document).height() > $(window).height()) {
      var scrollTop = $('html').scrollTop() ? $('html').scrollTop() : $('body').scrollTop();
      $('html').addClass('no-scroll').css('top', -scrollTop);
    }
  }

  function visibleScroll() {
    var scrollTop = parseInt($('html').css('top'));
    $('html').removeClass('no-scroll');
    $('html, body').scrollTop(-scrollTop);
  }

  // Masks
  (function () {
    var phone = $('input[type="tel"]:not(.search)');
    var date = $('input[name="birthday"]');

    phone.mask('+7 (999) 999-99-99', {
      autoclear: false });


    date.mask('99/99/9999', {
      autoclear: false });

  })();


  // Scroll to
  (function () {
    var arrow = $('.main__arrow-inner');
    var header = $('#header');
    var nav = header.find('.nav');
    var prizeId = document.getElementById('prize-info');
    var winnersId = document.getElementById('winners');
    var menu = header.find('.header__menu');

    arrow.on('click', function () {
      var target = $(this).attr('href');

      scrollTo(target);
      return false;
    });

    if (prizeId !== null && winnersId !== null) nav.on('click', '.nav__item--scroll', navScroll);

    function navScroll() {
      var target = $(this).attr('href');

      if (menu.hasClass('active') && $('html').hasClass('no-scroll')) {
        menu.removeClass('active');
        $('html').removeClass('no-scroll');
      }

      scrollTo(target);
      return false;
    }

    function scrollTo(el) {
      $('html, body').stop().animate({
        scrollTop: $(el).offset().top },
      600, function () {
        location.hash = el;
      });
    }
  })();


  // Validate
  (function () {
    var forms = $('form');

    $.each(forms, function () {
      $(this).validate({
        ignore: [],
        errorClass: 'error',
        validClass: 'success',
        rules: {
          phone: {
            required: true,
            phone: true },

          password: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          email: {
            required: true,
            email: true },

          name: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          surname: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          thems: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          card: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          citiy: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          birthday: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          registration: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          region: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          street: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          house: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } } },


        messages: {
          phone: 'Некорректный номер',
          rules: {
            required: '' },

          personalAgreement: {
            required: '' } },


        highlight: function highlight(el) {
          var parent = $(el).closest('.input-wrap');
          parent.addClass('error');
        },
        unhighlight: function unhighlight(el) {
          var parent = $(el).closest('.input-wrap');
          parent.removeClass('error');
        } });


      jQuery.validator.addMethod('phone', function (value, element) {
        return this.optional(element) || /\+7\s\(\d+\)\s\d{3}-\d{2}-\d{2}/.test(value);
      });

      jQuery.validator.addMethod('email', function (value, element) {
        return this.optional(element) || /\w+@[a-zA-Z_-]+?\.[a-zA-Z]{2,6}/.test(value);
      });

      jQuery.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || element.files[0].size <= param;
      });
    });
  })();
});