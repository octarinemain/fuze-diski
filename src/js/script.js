/* global SimpleBar */

'use strict';

$(document).ready(function() {	
	// Cookies
	(function() {
		let cookies = $('.cookies');

		if (getCookie('FUZE-diksi') === undefined) cookies.addClass('js-show');
		
		cookies.find('.btn--cookies').on('click', function(e) {
			e.preventDefault();
			cookies.removeClass('js-show');
			document.cookie = 'FUZE-diksi=value; path=/;';			
		});

		function getCookie(name) {
			let matches = document.cookie.match(new RegExp(
				`(?:^|; )${name.replace(/([.$?*|{}()[]\\\/\+^])/g, '\\$1')}=([^;]*)`
			));
			return matches ? decodeURIComponent(matches[1]) : undefined;
		}
	}) ();


	// Burger menu
	(function() {
		let header = $('#header');
		let burgerMenu = header.find('.burger-menu');

		burgerMenu.on('click', function(e) {
			e.preventDefault();
			let parent = $(this).parent();
			let menu = parent.find('.header__menu');
			menu.toggleClass('active');
			$('html').toggleClass('no-scroll');

			if (menu.hasClass('active') && $('html').hasClass('no-scroll')) {
				let width = $(window).width();

				$(window).on('resize.menu', function() {
					let widthResize = $(window).width();
					
					if (width !== widthResize) {
						menu.removeClass('active');
						$('html').removeClass('no-scroll');
						return $(window).off('resize.menu');
					}
				});
			} else {
				return $(window).off('resize.menu');
			}
		});

	}) ();


	// Select2
	(function() {
		let selects = $('select');

		selects.each(function() {
			let $this = $(this);
			let parent = $this.parent();
			let selectThems = parent.find('select[name="thems"]');
			let selectCitiy = parent.find('select[name="citiy"]');
			let selectRegion = parent.find('select[name="region"]');
			let selectStreet = parent.find('select[name="street"]');
			let selectHouse = parent.find('select[name="house"]');

			selectThems.select2({
				placeholder: 'Выберете тему',
				minimumResultsForSearch: -1,
				dropdownParent: parent
			});
	
			selectCitiy.select2({
				placeholder: 'Город',
				dropdownParent: parent,
				language: {
					noResults: function () {
						return 'Город не найден';
					}
				}
			});

			selectRegion.select2({
				placeholder: 'Регион',
				dropdownParent: parent,
				language: {
					noResults: function () {
						return 'Регион не найден';
					}
				}
			});

			selectStreet.select2({
				placeholder: 'Улица',
				dropdownParent: parent,
				language: {
					noResults: function () {
						return 'Улица не найдена';
					}
				}
			});

			selectHouse.select2({
				placeholder: '№ дома',
				dropdownParent: parent,
				language: {
					noResults: function () {
						return 'Дом не найден';
					}
				}
			});
		});		
	}) ();


	// Init custom scroll
	(function() {
		let table = document.getElementById('tables');
		let tableWinners = document.getElementById('table-winners');

		if (table !== null) {
			const tablesProfile = table.getElementsByClassName('profile-table');

			for (let i = 0; i < tablesProfile.length; i++) initCustomScroll(tablesProfile[i]);
		}

		if (tableWinners !== null) initCustomScroll(tableWinners);

		function initCustomScroll(el) {
			return new SimpleBar(el, {
				autoHide: false
			});
		}
	}) ();


	// Set prizes
	(function() {
		let tableChecks = document.getElementById('tables');

		function setPrizes() {
			const prizes = tableChecks.getElementsByClassName('profile-page__prize');
			const status = tableChecks.getElementsByClassName('profile-page__status-prize-inner');
		
			for (let i = 0; i < status.length; i++) status[i].setAttribute('data-prize', prizes[i].textContent);
		}
		
		if (tableChecks !== null) setPrizes();
	}) ();


	// Tabs
	(function() {
		let tabs = $('#tabs');
		let wrap = $('#tables');
		const tables = wrap.find('.profile-table');
		const tabsItem = tabs.find('li');

		tabs.on('click', 'li', function() {
			let $this = $(this);
			let i = $this.index();

			tabsItem.removeClass('active');
			$this.addClass('active');
			tables.removeClass('active').eq(i).addClass('active');
		});
	}) ();


	// Popups
	(function() {
		let menu = $('#header').find('.header__menu');
		let btnOpenPopups = $('.js-popup-button');
		let btnClosePopups = $('.js-close-popup');

		function intPopup() {
			btnOpenPopups.on('click', function(e) {
				e.preventDefault();
				$('.popup').removeClass('js-popup-show');
				let popupClass = `.${$(this).attr('data-popupshow')}`;
				$(popupClass).addClass('js-popup-show');

				if (!menu.hasClass('active')) hiddenScroll();
			});

			closePopup();
		}
	
		function closePopup() {
			btnClosePopups.on('click', function(e) {
				e.preventDefault();
				$('.popup').removeClass('js-popup-show');

				if (!menu.hasClass('active')) visibleScroll();
			});
		}
		intPopup();
	}) ();

	function hiddenScroll() {
		if ($(document).height() > $(window).height()) {
			let scrollTop = $('html').scrollTop() ? $('html').scrollTop() : $('body').scrollTop();
			$('html').addClass('no-scroll').css('top', -scrollTop);
		}
	}

	function visibleScroll() {
		let scrollTop = parseInt($('html').css('top'));
		$('html').removeClass('no-scroll');
		$('html, body').scrollTop(-scrollTop);
	}

	// Masks
	(function() {
		let phone = $('input[type="tel"]:not(.search)');
		let date = $('input[name="birthday"]');

		phone.mask('+7 (999) 999-99-99' ,{
			autoclear: false
		});

		date.mask('99/99/9999', {
			autoclear: false
		});
	}) ();


	// Scroll to
	(function() {
		let arrow = $('.main__arrow-inner');
		let header = $('#header');
		let nav = header.find('.nav');
		let prizeId = document.getElementById('prize-info');
		let winnersId = document.getElementById('winners');
		let menu = header.find('.header__menu');

		arrow.on('click', function() {
			let target = $(this).attr('href');

			scrollTo(target);
			return false;
		});

		if (prizeId !== null && winnersId !== null) nav.on('click', '.nav__item--scroll', navScroll);

		function navScroll() {
			let target = $(this).attr('href');

			if (menu.hasClass('active') && $('html').hasClass('no-scroll')) {
				menu.removeClass('active');
				$('html').removeClass('no-scroll');
			}
			
			scrollTo(target);
			return false;
		}

		function scrollTo(el) {
			$('html, body').stop().animate({
				scrollTop: $(el).offset().top
			}, 600, function() {
				location.hash = el;
			});
		}
	}) ();


	// Validate
	(function() {
		let forms = $('form');

		$.each(forms, function() {
			$(this).validate({
				ignore: [],
				errorClass: 'error',
				validClass: 'success',
				rules: {
					phone: {
						required: true,
						phone: true 
					},
					password: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						} 
					},
					email: {
						required: true,
						email: true 
					},
					name: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					surname: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					thems: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					card: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					citiy: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					birthday: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					registration: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					region: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					street: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					house: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
				},
				messages: {
					phone: 'Некорректный номер',
					rules: {
						required: '' 
					},
					personalAgreement: {
						required: '' 
					}
				},
				highlight: function(el) {
					let parent = $(el).closest('.input-wrap');
					parent.addClass('error');
				},
				unhighlight: function(el) {
					let parent = $(el).closest('.input-wrap');
					parent.removeClass('error');
				}
			});

			jQuery.validator.addMethod('phone', function (value, element) {
				return this.optional(element) || /\+7\s\(\d+\)\s\d{3}-\d{2}-\d{2}/.test(value);
			});
		
			jQuery.validator.addMethod('email', function (value, element) {
				return this.optional(element) || /\w+@[a-zA-Z_-]+?\.[a-zA-Z]{2,6}/.test(value);
			});

			jQuery.validator.addMethod('filesize', function (value, element, param) {
				return this.optional(element) || element.files[0].size <= param;
			});
		});
	}) ();
});